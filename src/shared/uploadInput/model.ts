import { createFactory } from '@withease/factories';
import { StoreWritable, createEvent } from 'effector';
type Props = {
  title: string;
  access: string;
  id: string;
  error: StoreWritable<'empty' | null>;
};
export const createUploadInputModel = createFactory(
  ({ access, id, title, error }: Props) => {
    const UploadFile = createEvent<FileList>();

    return { UploadFile, access, id, title, error };
  }
);

export type UploadInputModel = ReturnType<typeof createUploadInputModel>;
