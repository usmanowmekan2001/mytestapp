import { IconButton, TextField } from '@mui/material';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';

import { ChangeEvent } from 'react';
import { UploadInputModel } from './model';
import { useUnit } from 'effector-react';
// import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
type Props = {
  model: UploadInputModel;
};
export const Page = ({ model }: Props) => {
  const [UploadFile, error] = useUnit([model.UploadFile, model.error]);
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      UploadFile(e.target.files);
      // console.log();
      
    }
  };
  return (
    <div className="bg-white p-5 border text-center ">
      <TextField
        id={model.id}
        type={'file'}
        inputProps={{ accept: model.access }}
        className="opacity-0 invisible relative h-16"
        onChange={handleChange}
      />
      {/* <input
        accept={model.access}
        id={model.id}
        className="opacity-0 invisible relative h-16"
        onChange={handleChange}
        type="file"
      ></input> */}
      <label htmlFor={model.id} style={{ transform: 'translate(-20%,-50%)' }}>
        <IconButton
          color="primary"
          component="span"
          className="flex items-center text-center"
        >
          <p className="text-[20px] mr-3 my-auto">
            {error !== null ? (
              <span className="text-rose-700">File is Required</span>
            ) : (
              model.title
            )}
          </p>
          <CloudUploadIcon className="text-5xl" />
        </IconButton>
      </label>
    </div>
  );
};
