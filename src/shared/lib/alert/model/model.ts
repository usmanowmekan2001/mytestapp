import { createDomain } from 'effector';
export interface IAlert {
  alertText: string;
  alertStatus: string;
}
const error = createDomain();

export const setAlert = error.createEvent<IAlert>();

export const $alert = error 
  .createStore<IAlert>({ alertText: '', alertStatus: '' })
  .on(setAlert, (_, value) => value);

export const handleAlertMessage = (alert: IAlert) => {
  setAlert(alert);
  setTimeout(() => setAlert({ alertText: '', alertStatus: '' }), 3000);
};
