import DocViewer, { DocViewerRenderers } from '@cyntler/react-doc-viewer';

export const Page = ({ document }: { document: File[] | null }) => {
  if (document) {
    return (
      <div >
        <DocViewer
          documents={document.map((file) => ({
            uri: window.URL.createObjectURL(file),
            fileName: file.name,
          }))}
          pluginRenderers={DocViewerRenderers}
        />
      </div>
    );
  }
  return <div>Not Found</div>;
};
