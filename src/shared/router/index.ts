import { createEvent, createStore } from 'effector';
import { Router } from '@remix-run/router';
// import { createBrowserRouter } from 'react-router-dom';

export const changeRouter = createEvent<Router>();
export const $router = createStore<Router | null>(null).on(
  changeRouter,
  (_, route) => route
);
