import React, { ChangeEventHandler } from 'react';
interface IProps {
  className: string;
  type: string;
  label: string;
  name: string;
  disabled: boolean;
  value: string | number;
  onChange: ChangeEventHandler<HTMLInputElement>;
}
const CustomInput = (props: IProps) => {
  const { type, label, name, value, onChange, className, disabled } = props;
  return (
    <div className="form-floating mt-3">
      <input
        type={type}
        className={`form-control ${className}`}
        placeholder={label}
        name={name}
        value={value}
        onChange={onChange}
        onBlur={onChange}
        disabled={disabled}
      />
      <label htmlFor={label}>{label}</label>
    </div>
  );
};

export default CustomInput;
