import { invoke } from '@withease/factories';
import { createEvent, createStore } from 'effector';
import { createSidebarModel } from './components/Sidebar/model';
import { createHeaderModel } from './components/Header/model';

const createModel = () => {
  const SmallSidebar = createEvent();
  const $smallSidebar = createStore<boolean>(false).on(
    SmallSidebar,
    (value) => !value
  );
  const sideBarModel = invoke(createSidebarModel, { $smallSidebar });
  const headerModel = invoke(createHeaderModel, {
    $smallSidebar,
    SmallSidebar,
  });
  return { SmallSidebar, $smallSidebar, sideBarModel, headerModel };
};

export const pageModel = createModel();
