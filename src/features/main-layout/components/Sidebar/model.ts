import { createFactory } from '@withease/factories';
import {
  StoreWritable,
  attach,
  createEvent,
  createStore,
  sample,
} from 'effector';
import { $router } from '../../../../shared/router';
type Props = {
  $smallSidebar: StoreWritable<boolean>;
};

export const createSidebarModel = createFactory(({ $smallSidebar }: Props) => {
  const Select = createEvent<string>();
  const $select = createStore<string>('/').on(Select, (_, value) => value);
  const Route = createEvent();
  const routeFx = attach({
    source: { $router, $select },
    effect: ({ $router, $select }) => {
      console.log($select);
      if (!$router) throw new Error('ROuter is not available');
      $router?.navigate($select);
    },
  });
  sample({
    clock: Route,
    target: routeFx,
  });
  return { Select, $select, $smallSidebar, Route };
});

export type SidebarModel = ReturnType<typeof createSidebarModel>;
