import React, { useEffect } from 'react';
import { IoPersonOutline } from 'react-icons/io5';
import { AiOutlineDashboard } from 'react-icons/ai';
import styles from '../../ui/main-layout.module.css';
import { SidebarModel } from './model';
import { useUnit } from 'effector-react';
import { useLocation } from 'react-router-dom';

type Props = {
  model: SidebarModel;
};
export const Page = ({ model }: Props) => {
  const [smallSidebar, select, Select, Route] = useUnit([
    model.$smallSidebar,
    model.$select,
    model.Select,
    model.Route,
  ]);
  const location = useLocation();
  const currentLocation = location.pathname.split('/')[2];
  useEffect(() => {
    Select(currentLocation);
  }, []);

  // const navigate = useNavigate();
  const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    Select(e.currentTarget.id);
    Route();
    // navigate(e.currentTarget.id);
    console.log(e.currentTarget.id);
  };
  return (
    <div
      className={
        smallSidebar
          ? `w-20 duration-300 bg-[#001529] h-screen `
          : `${styles.BigSidebar} ${styles.sidebar}`
      }
    >
      <div className="h-16 bg-[#ffd333]"></div>
      <div>
        <div
          id="/"
          onClick={handleClick}
          className={
            select === '/'
              ? `flex items-center gap-2.5 p-2.5 rounded-md cursor-pointer mx-1 mt-2 mb-0 bg-[#1677ff]`
              : 'flex items-center gap-2.5 p-2.5 rounded-md cursor-pointer mx-1 mt-2 mb-0'
          }
        >
          <span
            className={smallSidebar ? styles.smallSidebarIcon : styles.itemIcon}
          >
            <AiOutlineDashboard />
          </span>
          {!smallSidebar && <div className={styles.itemName}>Main Page</div>}
        </div>
        <div
          id="/create"
          onClick={handleClick}
          className={
            select === '/create'
              ? `${styles.sidebarItem} ${styles.active}`
              : styles.sidebarItem
          }
        >
          <span
            className={smallSidebar ? styles.smallSidebarIcon : styles.itemIcon}
          >
            <IoPersonOutline />
          </span>
          {!smallSidebar && <div className={styles.itemName}>Create</div>}
        </div>
      </div>
    </div>
  );
};
