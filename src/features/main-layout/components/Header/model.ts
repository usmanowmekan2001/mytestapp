import { createFactory } from '@withease/factories';
import { EventCallable, StoreWritable } from 'effector';
type Props = {
  $smallSidebar: StoreWritable<boolean>;
  SmallSidebar: EventCallable<void>;
};

export const createHeaderModel = createFactory(
  ({ $smallSidebar, SmallSidebar }: Props) => {
    return { $smallSidebar, SmallSidebar };
  }
);

export type HeaderModel = ReturnType<typeof createHeaderModel>;
