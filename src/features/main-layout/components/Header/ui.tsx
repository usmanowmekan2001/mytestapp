import styles from '../../ui/main-layout.module.css';
import { AiOutlineMenuFold, AiOutlineMenuUnfold } from 'react-icons/ai';
import { HeaderModel } from './model';
import { useUnit } from 'effector-react';
type Props = {
  model: HeaderModel;
};
export const Page = ({ model }: Props) => {
  const [smallSidebar, setSmallSidebar] = useUnit([
    model.$smallSidebar,
    model.SmallSidebar,
  ]);
  const handleClick = () => {
    setSmallSidebar();
  };
  return (
    <div className="bg-white flex items-center grow justify-between">
      <div className={styles.headerIcon} onClick={handleClick}>
        {smallSidebar ? <AiOutlineMenuUnfold /> : <AiOutlineMenuFold />}
      </div>
    </div>
  );
};
