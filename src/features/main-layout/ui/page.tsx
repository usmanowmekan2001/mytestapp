import { Outlet, useLocation } from 'react-router-dom';

import { Sidebar } from '../components/Sidebar';
import { Header } from '../components/Header';
import { pageModel } from '../model';
import { useUnit } from 'effector-react';
import { useEffect } from 'react';
/* eslint-disable-next-line */
export interface MainLayoutProps {}

export function Page(props: MainLayoutProps) {
  const [setSelected, Route] = useUnit([
    pageModel.sideBarModel.Select,
    pageModel.sideBarModel.Route,
  ]);
  const location = useLocation();
  const currentLocation = location.pathname.split('/')[2];

  useEffect(() => {
    if (currentLocation === undefined) {
      setSelected('/');
      Route();
    } else {
      setSelected(currentLocation);
      Route();
    }
  }, []);

  return (
    <div className="min-h-screen overflow-hidden">
      <div className="flex grow bg-[#f5f5f5] h-screen">
        <div className="h-screen">
          <Sidebar model={pageModel.sideBarModel} />
        </div>
        <div className="grow">
          <Header model={pageModel.headerModel} />
          <div
            className="p-4 grow overflow-y-auto"
            style={{ height: 'calc(100vh - 73px)' }}
          >
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
}
