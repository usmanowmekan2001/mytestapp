import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';

import App from './app';
import { createBrowserRouter } from 'react-router-dom';
import { changeRouter } from '../shared/router';
import { MainPage } from '../pages/main';
import { CreatePage } from '../pages/create';
import { Layout } from '../features/main-layout';
import { EditPage } from '../pages/edit';
import { ViewPage } from '../pages/ViewBook';
import { Details } from '../pages/details';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        index: true,
        element: <MainPage />,
      },
      {
        path: 'create',
        element: <CreatePage />,
      },
      {
        path: '/:id',
        element: <Details />,
      },
      {
        path: 'update/:id',
        element: <EditPage />,
      },
      {
        path: '/view',
        element: <ViewPage />,
      },
    ],
  },
]);
const renderApp = async () => {
  changeRouter(router);

  const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
  );
  root.render(
    <StrictMode>
      <App />
      {/* <BrowserRouter>
      </BrowserRouter> */}
    </StrictMode>
  );
};
renderApp();
