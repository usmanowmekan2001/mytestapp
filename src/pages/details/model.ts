import { invoke } from '@withease/factories';
import { attach, createEvent, createStore, sample } from 'effector';
import { createUploadInputModel } from '../../shared/uploadInput/model';
import { $router } from '../../shared/router';
import { $editTingFile } from '../edit/model';
import { setAlert } from '../../shared/lib/alert/model/model';
import { IAlert } from '../../shared/lib/alert/alert';
import { $globalStore } from '../create/model';

export const createUpdateModel = () => {
  const $titleError = createStore<null | 'empty'>(null);
  const $descError = createStore<null | 'empty'>(null);
  const $imageInputError = createStore<null | 'empty'>(null);
  const $documentInputError = createStore<null | 'empty'>(null);

  const uploadImageModel = invoke(createUploadInputModel, {
    title: 'Upload Images',
    access: 'image/*',
    id: 'icon-button-photo',
    error: $imageInputError,
  });
  const uploadDocumentModel = invoke(createUploadInputModel, {
    title: 'Upload Document',
    access:
      'application/pdf,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel',
    id: 'icon-button-pdf',
    error: $documentInputError,
  });
  const $images = createStore<File[] | null>(null).on(
    uploadImageModel.UploadFile,
    (_, image) => Array.from(image)
  );
  const $document = createStore<File[] | null>(null).on(
    uploadDocumentModel.UploadFile,
    (_, document) => Array.from(document)
  );
  const $disableButton = createStore(false);

  const pageMounted = createEvent<number>();
  const FileName = createEvent<string>();
  const $fileName = createStore<string>('');
  const I = createEvent<number>();
  const $i = createStore<number>(0);

  const Description = createEvent<string>();
  const $description = createStore<string>('');
  const EditFile = createEvent();
  const DeleteFile = createEvent();




  const Spinner = createEvent();
  const $spinner = createStore(false);

  const handleAlertMessage = (alert: IAlert) => {
    setAlert(alert);
    setTimeout(() => setAlert({ alertText: '', alertStatus: '' }), 3000);
  };
  sample({
    clock: Spinner,
    source: $spinner,
    fn: (spinner) => !spinner,
    target: $spinner,
  });




  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.description;
    },
    target: Description,
  });
  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.title;
    },
    target: FileName,
  });
  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.id;
    },
    target: I,
  });
  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.file;
    },
    target: uploadDocumentModel.UploadFile,
  });
  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.images;
    },
    target: uploadImageModel.UploadFile,
  });

  sample({
    clock: FileName,
    target: $fileName,
  });
  sample({
    clock: I,
    target: $i,
  });
  sample({
    clock: Description,
    target: $description,
  });

  const routeFx = attach({
    source: { $router, $i },
    effect: ({ $router, $i }) => {
      if (!$router) throw new Error('ROuter is not available');
      $router?.navigate(`/update/${$i}`);
    },
  });
  const redirectMainFx = attach({
    source: { $router },
    effect: ({ $router }) => {
      if (!$router) throw new Error('ROuter is not available');
      $router?.navigate(`/`);
    },
  });
  sample({
    clock: EditFile,
    target: routeFx,
  });

  const deleteFx = attach({
    source: {
      $globalStore,
      $i,
    },
    effect: ({ $globalStore, $i }) => {
      if ($i !== null) {
        const store = $globalStore.splice($i, 1);
        return store;
      }
      throw new Error('The file error');
    },
  });

  
  const succesFx = attach({
    source: {},
    effect: () => {
      handleAlertMessage({
        alertText: 'Deleted Successfully',
        alertStatus: 'success',
      });
    },
  });

  sample({
    clock: DeleteFile,
    target: deleteFx,
  });
  sample({
    clock: DeleteFile,
    target: Spinner,
  });

  sample({
    clock: deleteFx.doneData,
    target: $globalStore,
  });
  sample({
    clock: deleteFx.done,
    target: Spinner,
  });
  sample({
    clock: deleteFx.done,
    target: redirectMainFx,
  });

  sample({
    clock: deleteFx.done,
    target: succesFx,
  });

  return {
    $disableButton,
    FileName,
    $fileName,
    Description,
    $description,
    $titleError,
    $descError,
    $imageInputError,
    $documentInputError,
    uploadImageModel,
    uploadDocumentModel,
    $images,
    $document,
    $i,
    pageMounted,
    EditFile,
    DeleteFile,
    $spinner,
  };
};

export const editPageModel = createUpdateModel();
