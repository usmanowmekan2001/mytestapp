import { Button, Typography } from '@mui/material';
import { useUnit } from 'effector-react';
import { useEffect } from 'react';

import img from '../../../shared/images/1.png';
import { editPageModel } from '../model';
import { Spinner } from '../../../shared/lib/spinner/spinner';
import { ViewBook } from '../../../shared/lib/viewBook';

export const Page = () => {
  const [
    images,
    document,
    fileName,
    description,
    pageMounted,
    EditFile,
    DeleteFile,
    spinner,
  ] = useUnit([
    editPageModel.$images,
    editPageModel.$document,
    editPageModel.$fileName,
    editPageModel.$description,
    editPageModel.pageMounted,
    editPageModel.EditFile,
    editPageModel.DeleteFile,
    editPageModel.$spinner,
  ]);
  console.log(images);
  useEffect(() => {
    pageMounted(1);
  }, []);

  const handleCreateFile = () => {
    EditFile();
  };
  const handleDeleteFile = () => {
    DeleteFile();
  };
  return (
    <div className="min-h-screen overflow-y-auto">
      <p className="text-center mt-3 text-lg">Edit Book</p>
      <div className="flex">
        <div className="flex flex-col gap-3 p-8 w-1/2">
          <Typography variant="h6"> 
            {fileName}
          </Typography>
          <Typography variant="subtitle1" >
            {description}
          </Typography>

          <div>
            {images !== null
              ? images.map((img, k) => {
                  if (img) {
                    return (
                      <div key={k} className="w-44 h-44 mt-2">
                        <img
                          src={window.URL.createObjectURL(img)}
                          alt="image"
                        />
                      </div>
                    );
                  }
                })
              : ''}
          </div>
          <div className=" ">
            {document ? (
              <div className="p-2 mt-3 w-40 cursor-pointer">
                <img src={img} alt="pdf" />
              </div>
            ) : (
              ''
            )}
          </div>
          <Button variant="contained" onClick={handleCreateFile}>
            Edit File
          </Button>
          {spinner ? (
            <div className='text-center'>
              <Spinner top={5} left={20} />
            </div>
          ) : (
            <Button
              variant="contained"
              style={{
                backgroundColor: 'red',
              }}
              onClick={handleDeleteFile}
            >
              Delete File
            </Button>
          )}
        </div>
        <div
          className="w-1/2 overflow-y-auto"
          style={{
            height: '80vh',
          }}
        >
          {document ? <ViewBook document={document} /> : ''}
        </div>
      </div>
    </div>
  );
};
