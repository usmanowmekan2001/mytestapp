import { Button, TextField } from '@mui/material';
import { useUnit } from 'effector-react';
import { ChangeEvent, useEffect } from 'react';

// import word from '../../../shared/images/Word1.png';
import img from '../../../shared/images/1.png';
import { UploadInput } from '../../../shared/uploadInput';
import { $editTingFile, editPageModel } from '../model';
import { ViewBook } from '../../../shared/lib/viewBook';
import { Spinner } from '../../../shared/lib/spinner/spinner';

export const Page = () => {
  const [
    images,
    document,
    fileName,
    description,
    ChangeFileName,
    changeDescription,
    CreateNewFile,
    titleError,
    descError,
    editTingFile,
    pageMounted,
    spinner,
    fileType,
  ] = useUnit([
    editPageModel.$images,
    editPageModel.$document,
    editPageModel.$fileName,
    editPageModel.$description,
    editPageModel.FileName,
    editPageModel.Description,
    editPageModel.EditFile,
    editPageModel.$titleError,
    editPageModel.$descError,
    $editTingFile,
    editPageModel.pageMounted,
    editPageModel.$spinner,
    editPageModel.$fileType,
  ]);
  console.log(images);
  useEffect(() => {
    pageMounted(1);
  }, []);

  const handleChangeTitle = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    ChangeFileName(e.target.value);
  };
  const handleChangeDescription = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    changeDescription(e.target.value);
  };

  const handleCreateFile = () => {
    CreateNewFile();
  };
  return (
    <div className="min-h-screen overflow-y-auto">
      <p className="text-center mt-3 text-lg">Edit Book</p>
      <div className="flex">
        <div className="flex flex-col gap-3 p-8 w-1/2">
          <TextField
            id="outlined-basic"
            label="Title"
            value={fileName}
            defaultValue={editTingFile.title}
            onChange={handleChangeTitle}
            variant="outlined"
            error={titleError !== null ? true : false}
            // helperText="Empty"
          />
          <TextField
            id="outlined-basic"
            label="Outlined"
            rows={4}
            multiline
            variant="outlined"
            value={description}
            defaultValue={description}
            onChange={handleChangeDescription}
            error={descError !== null ? true : false}
            // helperText="Empty"
          />
          <div>
            <UploadInput model={editPageModel.uploadImageModel} />
            {images !== null
              ? images.map((img, k) => {
                  if (img) {
                    return (
                      <div key={k} className="w-44 h-44 mt-2">
                        <img
                          src={window.URL.createObjectURL(img)}
                          alt="image"
                        />
                      </div>
                    );
                  }
                })
              : ''}
          </div>
          <div className="mt-2">
            <UploadInput model={editPageModel.uploadDocumentModel} />
            {document ? (
              fileType === 'pdf' ? (
                <div className="p-2 mt-3 w-40 cursor-pointer">
                  <img src={img} alt="pdf" />
                </div>
              ) : (
                // ) : fileType === 'docx' ? (
                //   <div className="p-2 mt-3 w-40 cursor-pointer">
                //     <div className="p-2 mt-3 w-40 cursor-pointer">
                //       <img src={word} alt="pdf" />
                //     </div>
                //   </div>
                <div className="text-center mt-2" style={{ color: 'red' }}>
                  Type of file should be only pdf
                </div>
              )
            ) : (
              ''
            )}
          </div>
          {spinner ? (
            <div className="text-center">
              <Spinner top={5} left={20} />
            </div>
          ) : (
            <Button variant="contained" onClick={handleCreateFile}>
              Edit File
            </Button>
          )}
        </div>
        <div
          className="w-1/2 overflow-y-auto"
          style={{
            height: '150vh',
          }}
        >
          {document && fileType === 'pdf' ? (
            <ViewBook document={document} />
          ) : (
            ''
          )}
        </div>
      </div>
    </div>
  );
};
