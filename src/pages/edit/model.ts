import { invoke } from '@withease/factories';
import { attach, createEvent, createStore, sample } from 'effector';
import { reset } from 'patronum';
import { createUploadInputModel } from '../../shared/uploadInput/model';
import { $router } from '../../shared/router';
import { $globalStore } from '../create/model';
import { InitialState } from '../main/model';
import { setAlert } from '../../shared/lib/alert/model/model';
import { IAlert } from '../../shared/lib/alert/alert';

const initialState = {
  id: 0,
  title: '',
  description: '',
  images: [],
  file: null,
};
export const $editTingFile = createStore<InitialState>(initialState);
export const EditFile = createEvent<InitialState>();

export const createUpdateModel = () => {
  const $titleError = createStore<null | 'empty'>(null);
  const $descError = createStore<null | 'empty'>(null);
  const $imageInputError = createStore<null | 'empty'>(null);
  const $documentInputError = createStore<null | 'empty'>(null);

  const uploadImageModel = invoke(createUploadInputModel, {
    title: 'Upload Images',
    access: 'image/*',
    id: 'icon-button-photo',
    error: $imageInputError,
  });
  const uploadDocumentModel = invoke(createUploadInputModel, {
    title: 'Upload Document',
    access:
      'application/pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    id: 'icon-button-pdf',
    error: $documentInputError,
  });
  const $images = createStore<File[] | null>(null).on(
    uploadImageModel.UploadFile,
    (_, image) => Array.from(image)
  );
  const $document = createStore<File[] | null>(null).on(
    uploadDocumentModel.UploadFile,
    (_, document) => Array.from(document)
  );
  const $disableButton = createStore(false);

  const pageMounted = createEvent<number>();
  const FileName = createEvent<string>();
  const $fileName = createStore<string>('');
  const I = createEvent<number>();
  const $i = createStore<number>(0);

  const Description = createEvent<string>();
  const $description = createStore<string>('');

  const ResetStores = createEvent();
  const EditFile = createEvent();

  const Spinner = createEvent();
  const $spinner = createStore(false);

  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.description;
    },
    target: Description,
  });
  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.title;
    },
    target: FileName,
  });
  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.id;
    },
    target: I,
  });
  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.file;
    },
    target: uploadDocumentModel.UploadFile,
  });
  sample({
    clock: pageMounted,
    source: $editTingFile,
    fn: (file) => {
      return file.images;
    },
    target: uploadImageModel.UploadFile,
  });

  sample({
    clock: FileName,
    target: $fileName,
  });
  sample({
    clock: I,
    target: $i,
  });
  sample({
    clock: Description,
    target: $description,
  });

  sample({
    clock: EditFile,
    source: $fileName,
    fn: (name) => {
      if (isEmpty(name)) return 'empty';
      return null;
    },
    target: $titleError,
  });

  sample({
    clock: EditFile,
    source: $document,
    fn: (name) => {
      if (name === null) return 'empty';
      return null;
    },
    target: $documentInputError,
  });

  sample({
    clock: EditFile,
    source: $images,
    fn: (name) => {
      if (name === null) return 'empty';
      return null;
    },
    target: $imageInputError,
  });

  sample({
    clock: EditFile,
    source: $description,
    fn: (name) => {
      if (isEmpty(name)) return 'empty';
      return null;
    },
    target: $descError,
  });

  // check file type
  const $file = createStore<FileList | null>(null);
  const $fileType = createStore<string>('');
  sample({
    clock: uploadDocumentModel.UploadFile,
    target: $file,
  });
  sample({
    clock: uploadDocumentModel.UploadFile,
    source: $file,
    fn: (file) => {
      if (file) {
        console.log(file[0]);

        return file[0].name.split('.')[1];
      }
      return '';
    },
    target: $fileType,
  });

  const editF = attach({
    source: {
      $globalStore,
      $fileName,
      $description,
      $images,
      $document,
      $router,
      $i,
    },
    effect: ({
      $globalStore,
      $fileName,
      $description,
      $images,
      $document,
      $router,
      $i,
    }) => {
      if (
        $fileName?.trim().length !== 0 &&
        $description?.trim().length !== 0 &&
        $images !== null &&
        $document !== null
      ) {
        const store = $globalStore.splice($i, 1);

        store.push({
          id: $i,
          title: $fileName,
          description: $description,
          images: $images,
          file: $document,
        });

        return store;
      }
      throw new Error('The file error');
    },
  });
  sample({
    clock: EditFile,
    target: editF,
  });
  sample({
    clock: EditFile,
    source: { $titleError, $descError, $imageInputError, $documentInputError },
    fn: ({
      $titleError,
      $descError,
      $imageInputError,
      $documentInputError,
    }) => {
      if (
        $titleError !== null &&
        $descError !== null &&
        $imageInputError !== null &&
        $documentInputError !== null
      ) {
        return true;
      }
      return false;
    },
    target: Spinner,
  });
  sample({
    clock: editF.doneData,
    target: $globalStore,
  });
  const routeFx = attach({
    source: { $router },
    effect: ({ $router }) => {
      if (!$router) throw new Error('ROuter is not available');
      $router?.navigate('/');
    },
  });
  sample({
    clock: editF.done,
    target: Spinner,
  });
  sample({
    clock: editF.done,
    target: routeFx,
  });
  const handleAlertMessage = (alert: IAlert) => {
    setAlert(alert);
    setTimeout(() => setAlert({ alertText: '', alertStatus: '' }), 3000);
  };

  const succesFx = attach({
    source: {},
    effect: () => {
      handleAlertMessage({
        alertText: 'File Editted Successfully',
        alertStatus: 'success',
      });
    },
  });

  sample({
    clock: editF.done,
    target: succesFx,
  });

  reset({
    clock: editF.done,
    target: [
      $titleError,
      $descError,
      $documentInputError,
      $imageInputError,
      $description,
      $document,
      $images,
      $fileName,
    ],
  });

  function isEmpty(input: string) {
    return input.trim().length === 0;
  }
  return {
    $disableButton,
    FileName,
    $fileName,
    Description,
    $description,
    ResetStores,
    EditFile,
    $titleError,
    $descError,
    $imageInputError,
    $documentInputError,
    uploadImageModel,
    uploadDocumentModel,
    $images,
    $document,
    $i,
    pageMounted,
    $spinner,
    $fileType,
  };
};

export const editPageModel = createUpdateModel();
