import { attach, createEvent, createStore, sample } from 'effector';
import { $router } from '../../shared/router';
import { reset } from 'patronum';

export const $view = createStore<File[] | null>(null);
export const $prevRoute = createStore<string | null>(null);
const createModal = () => {
  const Exit = createEvent();
  const routeViewFx = attach({
    source: { $router, $prevRoute },
    effect: ({ $router, $prevRoute }) => {
      if (!$router && $prevRoute === null)
        throw new Error('ROuter is not available');
      $router?.navigate(-1);
    },
  });
  sample({
    clock: Exit,
    target: routeViewFx,
  });
  reset({
    clock: Exit,
    target: $view,
  });
  return { Exit };
};
export const viewModel = createModal();
