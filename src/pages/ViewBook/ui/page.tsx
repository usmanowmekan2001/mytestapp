import { useUnit } from 'effector-react';
import { ViewBook } from '../../../shared/lib/viewBook';
import { $view, viewModel } from '../model';

export const Page = () => {
  const [view, Exit] = useUnit([$view, viewModel.Exit]);
  const handleExit = () => {
    Exit();
  };
  return (
    <div>
      <div
        className="fixed top-2 right-2 z-50 text-black cursor-pointer"
        onClick={handleExit}
      >
        exit
      </div>
      <div className="relative top-0 left-0 right-0 h-screen z-11">
        <ViewBook document={view} />
      </div>
    </div>
  );
};
