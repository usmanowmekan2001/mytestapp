import { Button, TextField } from '@mui/material';
import { useUnit } from 'effector-react';
import { ChangeEvent, useEffect } from 'react';

import img from '../../../shared/images/1.png';
// import word from '../../../shared/images/Word1.png';
import { UploadInput } from '../../../shared/uploadInput';
import { createPageModel } from '../model';
import { useLocation } from 'react-router-dom';
import { ViewBook } from '../../../shared/lib/viewBook';
import { Spinner } from '../../../shared/lib/spinner/spinner';
export const Page = () => {
  const [
    images,
    document,
    fileName,
    description,
    ChangeFileName,
    changeDescription,
    CreateNewFile,
    titleError,
    descError,
    resetStores,
    fileType,
    spinner,
  ] = useUnit([
    createPageModel.$images,
    createPageModel.$document,
    createPageModel.$fileName,
    createPageModel.$description,
    createPageModel.FileName,
    createPageModel.Description,
    createPageModel.CreateFile,
    createPageModel.$titleError,
    createPageModel.$descError,
    createPageModel.ResetStores,
    createPageModel.$fileType,
    createPageModel.$spinner,
  ]);
  console.log(fileName);
  useEffect(() => {
    resetStores();
  }, []);

  const location = useLocation();
  const currentLocation = location.pathname.split('/')[2];
  console.log(currentLocation);

  const handleChangeTitle = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    ChangeFileName(e.target.value);
  };
  const handleChangeDescription = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    changeDescription(e.target.value);
  };

  const handleCreateFile = () => {
    CreateNewFile();
  };

  return (
    <div className="min-h-screen overflow-y-auto">
      <p className="text-center mt-3 text-lg">Create Book</p>
      <div className="flex">
        <div className="flex flex-col gap-3 p-8 w-1/2">
          <TextField
            id="outlined-basic"
            label="Title"
            value={fileName}
            onChange={handleChangeTitle}
            variant="outlined"
            error={titleError !== null ? true : false}
            // helperText="Empty"
          />
          <TextField
            id="outlined-basic"
            label="Outlined"
            rows={4}
            multiline
            variant="outlined"
            value={description}
            onChange={handleChangeDescription}
            error={descError !== null ? true : false}
            // helperText="Empty"
          />
          <div>
            <UploadInput model={createPageModel.uploadImageModel} />
            {images !== null
              ? images.map((img, k) => {
                  if (img) {
                    return (
                      <div>
                        <div key={k} className="w-44 h-44 mt-2">
                          <img
                            src={window.URL.createObjectURL(img)}
                            alt="image"
                          />
                        </div>
                        {/* <div className="absolute top-2 right-2 cursor-pointer">
                          Xmnn
                        </div> */}
                      </div>
                    );
                  }
                })
              : ''}
          </div>
          <div className="">
            <UploadInput model={createPageModel.uploadDocumentModel} />
            {document ? (
              fileType === 'pdf' ? (
                <div className="p-2 mt-3 w-40 cursor-pointer">
                  <img src={img} alt="pdf" />
                </div>
              ) : (
                // ) : fileType === 'docx' ? (
                //   <div className="p-2 mt-3 w-40 cursor-pointer">
                //     <div className="p-2 mt-3 w-40 cursor-pointer">
                //       <img src={word} alt="pdf" />
                //     </div>
                //   </div>
                <div className="text-center mt-2" style={{ color: 'red' }}>
                  Type of file should be only pdf
                </div>
              )
            ) : (
              ''
            )}
          </div>
          {spinner ? (
            <div className="text-center">
              <Spinner top={5} left={20} />
            </div>
          ) : (
            <Button variant="contained" onClick={handleCreateFile}>
              Add File
            </Button>
          )}
        </div>
        <div className="w-1/2 overflow-y-auto" style={{ height: '150vh' }}>
          {document && fileType === 'pdf' ? (
            <ViewBook document={document} />
          ) : (
            ''
          )}
        </div>
      </div>
    </div>
  );
};
