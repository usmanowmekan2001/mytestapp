import { invoke } from '@withease/factories';
import { attach, createEvent, createStore, sample } from 'effector';
import { reset } from 'patronum';
import { createUploadInputModel } from '../../shared/uploadInput/model';
import { $router } from '../../shared/router';
import { InitialState } from '../main/model';
import { IAlert } from '../../shared/lib/alert/alert';
import { setAlert } from '../../shared/lib/alert/model/model';
// import { $prevRoute, $view } from '../../shared/lib/viewBook/model';

// const initialState = {
//   id: null,
//   title: null,
//   description: null,
//   images: [],
//   file: null,
// };

export const $globalStore = createStore<InitialState[]>([]);

const createModel = () => {
  const $titleError = createStore<null | 'empty'>(null);
  const $descError = createStore<null | 'empty'>(null);
  const $imageInputError = createStore<null | 'empty'>(null);
  const $documentInputError = createStore<null | 'empty'>(null);

  const uploadImageModel = invoke(createUploadInputModel, {
    title: 'Upload Images',
    access: 'image/*',
    id: 'icon-button-photo',
    error: $imageInputError,
  });
  const uploadDocumentModel = invoke(createUploadInputModel, {
    title: 'Upload Document',
    access:
      'application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    id: 'icon-button-pdf',
    error: $documentInputError,
  });
  const $images = createStore<File[] | null>(null).on(
    uploadImageModel.UploadFile,
    (_, image) => Array.from(image)
  );
  const $document = createStore<File[] | null>(null).on(
    uploadDocumentModel.UploadFile,
    (_, document) => Array.from(document)
  );
  const $disableButton = createStore(false);
  const FileName = createEvent<string>();
  const $fileName = createStore<string>('');
  const Description = createEvent<string>();
  const $description = createStore<string>('');
  const ResetStores = createEvent();
  const CreateFile = createEvent();

  const Spinner = createEvent();
  const $spinner = createStore(false);

  const handleAlertMessage = (alert: IAlert) => {
    setAlert(alert);
    setTimeout(() => setAlert({ alertText: '', alertStatus: '' }), 3000);
  };

  // sample({
  //   clock: Spinner,
  //   source: $spinner,
  //   fn: (spinner) => !spinner,
  //   target: $spinner,
  // });

  sample({
    clock: FileName,
    target: $fileName,
  });
  sample({
    clock: Description,
    target: $description,
  });

  sample({
    clock: CreateFile,
    source: $fileName,
    fn: (name) => {
      if (isEmpty(name)) return 'empty';
      return null;
    },
    target: $titleError,
  });

  sample({
    clock: CreateFile,
    source: $document,
    fn: (name) => {
      if (name === null) return 'empty';
      return null;
    },
    target: $documentInputError,
  });

  sample({
    clock: CreateFile,
    source: $images,
    fn: (name) => {
      if (name === null) return 'empty';
      return null;
    },
    target: $imageInputError,
  });

  sample({
    clock: CreateFile,
    source: $description,
    fn: (name) => {
      if (isEmpty(name)) return 'empty';
      return null;
    },
    target: $descError,
  });

  const createF = attach({
    source: {
      $globalStore,
      $fileName,
      $description,
      $images,
      $document,
      $router,
    },
    effect: ({
      $globalStore,
      $fileName,
      $description,
      $images,
      $document,
      $router,
    }) => {
      if (
        $fileName.trim().length !== 0 &&
        $description.trim().length !== 0 &&
        $images !== null &&
        $document !== null
      ) {
        const newState = {
          id: $globalStore.length + 1,
          title: $fileName,
          description: $description,
          images: $images,
          file: $document,
        };
        return [...$globalStore, newState];
      }
      throw new Error('The file error');
    },
  });

  sample({
    clock: CreateFile,
    source: { $titleError, $descError, $imageInputError, $documentInputError },
    fn: ({
      $titleError,
      $descError,
      $imageInputError,
      $documentInputError,
    }) => {
      if (
        $titleError !== null &&
        $descError !== null &&
        $imageInputError !== null &&
        $documentInputError !== null
      ) {
        return true;
      }
      return false;
    },
    target: Spinner,
  });
  sample({
    clock: createF.done,
    target: Spinner,
  });

  // check file type
  const $file = createStore<FileList | null>(null);
  const $fileType = createStore<string>('');
  sample({
    clock: uploadDocumentModel.UploadFile,
    target: $file,
  });
  sample({
    clock: uploadDocumentModel.UploadFile,
    source: $file,
    fn: (file) => {
      if (file) {
        console.log(file[0]);

        return file[0].name.split('.')[1];
      }
      return '';
    },
    target: $fileType,
  });

  const routeFx = attach({
    source: { $router },
    effect: ({ $router }) => {
      if (!$router) throw new Error('ROuter is not available');
      $router?.navigate('/');
    },
  });
  sample({
    clock: CreateFile,
    target: createF,
  });
  sample({
    clock: createF.doneData,
    target: $globalStore,
  });

  sample({
    clock: createF.done,
    target: routeFx,
  });

  const succesFx = attach({
    source: {},
    effect: () => {
      handleAlertMessage({
        alertText: 'Created Successfully',
        alertStatus: 'success',
      });
    },
  });

  sample({
    clock: createF.done,
    target: succesFx,
  });

  reset({
    clock: ResetStores,
    target: [
      $titleError,
      $descError,
      $documentInputError,
      $imageInputError,
      $description,
      $document,
      $images,
      $fileName,
    ],
  });

  function isEmpty(input: string) {
    return input.trim().length === 0;
  }
  return {
    $disableButton,
    FileName,
    $fileName,
    Description,
    $description,
    ResetStores,
    CreateFile,
    $titleError,
    $descError,
    $imageInputError,
    $documentInputError,
    uploadImageModel,
    uploadDocumentModel,
    $images,
    $document,
    $fileType,
    $spinner,
  };
};

export const createPageModel = createModel();
