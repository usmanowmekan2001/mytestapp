import { attach, createEvent, createStore, sample } from 'effector';
import { $router } from '../../shared/router';
import { $globalStore } from '../create/model';
import { $editTingFile } from '../edit/model';
import { $view } from '../ViewBook/model';
export type InitialState = {
  id: number;
  title: string;
  description: string;
  images: File[];
  file: File[] | null;
};
export const $id = createStore<number>(0);

const createModel = () => {
  const File = createEvent<number>();
  const View = createEvent<File[] | null>();
  const CurrentRoute = createEvent<string | null>();

  sample({
    clock: File,
    target: $id,
  });

  sample({
    clock: File,
    source: { $globalStore, $id },
    fn: ({ $globalStore, $id }) => {
      return $globalStore[$id - 1];
    },
    target: $editTingFile,
  });
  const routeFx = attach({
    source: { $router, $id },
    effect: ({ $router, $id }) => {
      if (!$router) throw new Error('ROuter is not available');
      $router?.navigate(`/${$id}`);
    },
  });

  sample({
    clock: File,
    target: routeFx,
  });

  const routeViewFx = attach({
    source: $router,
    effect: (router) => {
      if (!$router) throw new Error('ROuter is not available');
      router?.navigate(`/view`);
    },
  });

  sample({
    clock: View,
    target: $view,
  });

  sample({
    clock: View,
    target: routeViewFx,
  });

  // sample({
  //   clock: CurrentRoute,
  //   target: $prevRoute,
  // });
  return { File, View, CurrentRoute };
};

export const MainPageModel = createModel();
