import { Typography } from '@mui/material';
import { useUnit } from 'effector-react';
import { $globalStore } from '../../create/model';
import { MainPageModel } from '../model';
import view from '../../../shared/images/view.svg';
import { useLocation } from 'react-router-dom';
export const Page = () => {
  const [globalStore, File, View, CurrentRoute] = useUnit([
    $globalStore,
    MainPageModel.File,
    MainPageModel.View,
    MainPageModel.CurrentRoute,
  ]);
  const location = useLocation();
  const currentLocation = location.pathname.split('/')[2];
  const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    File(Number(e.currentTarget.id));
  };
  const handleClickView = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.stopPropagation();
    const id = Number(e.currentTarget.id);
    View(globalStore[id].file);
    CurrentRoute(currentLocation);
  };
  return (
    <main className={`flex flex-wrap p-4 overflow-hidden`}>
      {globalStore.map((e, k) => {
        return (
          <div
            id={String(e.id)}
            key={k}
            onClick={handleClick}
            className="relative w-64 h-80 border p-2 flex flex-col justify-between rounded overflow-hidden"
          >
            <div key={k} className="">
              <img src={URL.createObjectURL(e.images[0])} alt="image" />
            </div>
            <Typography variant="h6">{e.title}</Typography>
            <Typography variant="body1">{e.description}</Typography>
            <div
              className=""
              style={{
                position: 'absolute',
                top: '10%',
                right: '10px',
                transition: 'all 0.3s ease-in-out',
                zIndex: 10,
                cursor: 'pointer',
              }}
            >
              <div
                className=""
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  gap: '15px',
                }}
              >
                <div
                  className=""
                  onClick={handleClickView}
                  style={{ border: 'none', background: 'transparent' }}
                >
                  <img src={view} alt="view" />
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </main>
  );
};
